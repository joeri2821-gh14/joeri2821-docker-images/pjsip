# PJSIP ARM64 #
This projects builds PJSIP (a voip library) in a docker image for Linux ARM64 platform (RaspberryPi 3/4).
* Base image: debian:buster 
* Source: https://www.pjsip.org/

The image is pushed to Github, as `joeri2821/pjsip`

Upon version update: don't forget to update the image tag in `docker/version.sh`
