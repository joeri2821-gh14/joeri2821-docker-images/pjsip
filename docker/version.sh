IMAGE_NAME=pjsip
if [ -z "$GIT_TAG" ]; then
	echo "No GIT_TAG available, use latest as docker image tag..."
	IMAGE_VERSION='latest'
else
	echo "Using GIT_TAG as docker image tag..."
	IMAGE_VERSION=$GIT_TAG
fi
IMAGE_TAG=$IMAGE_NAME\:$IMAGE_VERSION

